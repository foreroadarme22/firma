import javax.crypto.Cipher;
import java.io.FileInputStream;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;

public class VerificadorFirmaYDescifrado {
    private final String algoritmo = "RSA";
    private PublicKey publicKey;
    private byte[] mensajeCifrado;
    private byte[] firma;

    public VerificadorFirmaYDescifrado(byte[] mensajeCifrado, byte[] firma) {
        this.mensajeCifrado = mensajeCifrado;
        this.firma = firma;
    }

    public void verificarYDescifrar() {
        try {
            // Leer clave pública
            FileInputStream publicFis = new FileInputStream("publicKey");
            byte[] publicBytes = publicFis.readAllBytes();
            X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(publicBytes);
            KeyFactory keyFactory = KeyFactory.getInstance(algoritmo);
            publicKey = keyFactory.generatePublic(publicSpec);

            // Verificar firma
            Signature signature = Signature.getInstance("SHA256withRSA");
            signature.initVerify(publicKey);
            signature.update(mensajeCifrado);
            boolean verificado = signature.verify(firma);

            if (verificado) {
                // Imprimir mensaje cifrado
                System.out.println("Mensaje cifrado: " + new String(mensajeCifrado));
            } else {
                System.out.println("La firma no se ha verificado correctamente.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
