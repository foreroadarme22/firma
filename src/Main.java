import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Generar claves RSA y guardarlas en archivos
        GeneradorClaves generador = new GeneradorClaves();
        generador.generar();

        // Cifrar un mensaje y firmarlo digitalmente
        CifradoYFirma cifradorFirmador = new CifradoYFirma();
        cifradorFirmador.cifrarYFirmar();

        // Leer mensaje cifrado y firma cifrada desde archivos
        byte[] mensajeCifrado = leerArchivo("mensajeCifrado");
        byte[] firmaCifrada = leerArchivo("firmaCifrada");

        // Descifrar la firma y verificarla
        VerificadorFirmaYDescifrado verificador = new VerificadorFirmaYDescifrado(mensajeCifrado, firmaCifrada);
        verificador.verificarYDescifrar();

        scanner.close();
    }

    // Método para leer archivos
    private static byte[] leerArchivo(String nombreArchivo) {
        try {
            //Aquí se crea un nuevo objeto//
            FileInputStream fis = new FileInputStream(nombreArchivo);
            //El método devuelve un array de bytes//
            byte[] bytes = fis.readAllBytes();
            fis.close();
            return bytes;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
