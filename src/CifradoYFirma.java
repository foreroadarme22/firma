import javax.crypto.Cipher;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Scanner;

public class CifradoYFirma {
    private final String algoritmo = "RSA";
    private PrivateKey privateKey;
    private String mensajeNormal;
    private byte[] mensajeCifrado;
    private byte[] firma;

    public void cifrarYFirmar() {
        try {
            // Leer mensaje de la terminal
            Scanner scanner = new Scanner(System.in);
            System.out.println("Introduce el mensaje a cifrar:");
            mensajeNormal = scanner.nextLine();

            // Leer clave privada
            FileInputStream privateFis = new FileInputStream("privateKey");
            byte[] privateBytes = privateFis.readAllBytes();
            privateFis.close();

            KeyFactory keyFactory = KeyFactory.getInstance(algoritmo);
            privateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(privateBytes));

            // Cifrado del mensaje
            Cipher cipher = Cipher.getInstance(algoritmo);
            cipher.init(Cipher.ENCRYPT_MODE, privateKey);
            mensajeCifrado = cipher.doFinal(mensajeNormal.getBytes());

            // Firma digital del mensaje
            Signature signature = Signature.getInstance("SHA256withRSA");
            signature.initSign(privateKey);
            signature.update(mensajeCifrado);
            firma = signature.sign();

            // Guardar mensaje cifrado y firma
            FileOutputStream mensajeFos = new FileOutputStream("mensajeCifrado");
            mensajeFos.write(mensajeCifrado);
            mensajeFos.close();

            FileOutputStream firmaFos = new FileOutputStream("firmaCifrada");
            firmaFos.write(firma);
            firmaFos.close();

            System.out.println("Mensaje cifrado y firma generados correctamente.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
