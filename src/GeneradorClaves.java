
import java.io.FileOutputStream;
import java.security.*;


public class GeneradorClaves {
    private final String algoritmo = "RSA";
    private PublicKey publicKey;
    private PrivateKey privateKey;

    public void generar() {
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(algoritmo);
            keyPairGenerator.initialize(2048);

            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            publicKey = keyPair.getPublic();
            privateKey = keyPair.getPrivate();

            // Guardar claves en archivos
            FileOutputStream publicFos = new FileOutputStream("publicKey");
            publicFos.write(publicKey.getEncoded());
            publicFos.close();

            FileOutputStream privateFos = new FileOutputStream("privateKey");
            privateFos.write(privateKey.getEncoded());
            privateFos.close();

            System.out.println("Claves generadas y guardadas correctamente.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}